import { Request, Response } from 'express';
import { createUser, getAllUsers, getUserByUserName, deleteUser, updateUser } from '../models/userModel';

// Create User
export const createUserController = async (req: Request, res: Response) => {
  console.log('Request Body:', req.body);
  try {
    const user = req.body;
    const createdUser = await createUser(user);
    res.status(201).json(createdUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};

// Get All Users
export const getAllUsersController = async (req: Request, res: Response) => {
  console.log('GGGGGGGGGGGGGGGGGGG');
  
  try {
    const users = await getAllUsers();
    res.status(200).json(users);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};

// Get User by Username
export const getUserByUserNameController = async (req: Request, res: Response) => {
  try {
    const { userName } = req.params;
    const user = await getUserByUserName(userName);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({ message: 'User not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};

// Delete User
export const deleteUserController = async (req: Request, res: Response) => {
  try {
    const { userName } = req.params;
    const user = await deleteUser(userName);
    if (user) {
      res.status(200).json({ message: 'User deleted' });
    } else {
      res.status(404).json({ message: 'User not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};

// Update User
export const updateUserController = async (req: Request, res: Response) => {
  try {
    const { userName } = req.params;
    const updatedData = req.body;
    const updatedUser = await updateUser(userName, updatedData);
    if (updatedUser) {
      res.status(200).json(updatedUser);
    } else {
      res.status(404).json({ message: 'User not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};
