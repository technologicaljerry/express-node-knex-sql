import pool from '../config/data-source';

interface User {
    first_name: string;
    last_name: string;
    user_name: string;
    email: string;
    password: string;
    contact: string;
  }
  

  export const createUser = async (users: User) => {
    const { first_name, last_name, user_name, email, password, contact } = users;
  
    try {
      const result = await pool.query(
        `INSERT INTO users (first_name, last_name, user_name, email, password, contact) 
         VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
        [first_name, last_name, user_name, email, password, contact]
      );
  
      return result.rows[0];
    } catch (error) {
      console.error('Error creating user:', error);
      throw new Error('Failed to create user');
    }
  };

export const getAllUsers = async () => {
    const result = await pool.query('SELECT * FROM users');
    return result.rows;
};

export const getUserByUserName = async (user_name: string) => {
    const result = await pool.query('SELECT * FROM users WHERE username = $1', [user_name]);
    return result.rows[0];
};

export const deleteUser = async (user_name: string) => {
    const result = await pool.query('DELETE FROM users WHERE username = $1 RETURNING *', [user_name]);
    return result.rows[0];
};

export const updateUser = async (user_name: string, updatedData: any) => {
    const { first_name, last_name, email, contact } = updatedData;
    const result = await pool.query(
        `UPDATE users SET first_name = $1, last_name = $2, email = $3, contact = $4 WHERE username = $5 RETURNING *`,
        [first_name, last_name, email, contact, user_name]
    );
    return result.rows[0];
};
