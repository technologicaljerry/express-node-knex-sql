import { Router } from 'express';
import {
  createUserController,
  getAllUsersController,
  getUserByUserNameController,
  deleteUserController,
  updateUserController,
} from '../controllers/userController';

const router = Router();

router.post('/users', createUserController);
router.get('/users', getAllUsersController);
router.get('/users/:userName', getUserByUserNameController);
router.delete('/users/:userName', deleteUserController);
router.put('/users/:userName', updateUserController);

export default router;
