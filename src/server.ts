import app from './app';
import pool from './config/data-source'; // Import the PostgreSQL pool
import dotenv from 'dotenv';

dotenv.config();

const PORT = parseInt(process.env.SERVER_PORT || '3000', 10);
const SWAGGER_UI_URL = process.env.SWAGGER_UI_URL || 'api-docs';

async function startServer() {
    try {
        // Test the database connection
        await pool.query('SELECT 1');
        console.log('Database connected successfully');

        // Start the Express server
        app.listen(PORT, () => {
            console.log(`Server running on http://localhost:${PORT}`);
            console.log(`Swagger docs available at http://localhost:${PORT}/${SWAGGER_UI_URL}`);
        });
    } catch (error) {
        console.error('Error connecting to the database:', error);
        process.exit(1); // Exit with failure if the database connection fails
    }
}

startServer();


