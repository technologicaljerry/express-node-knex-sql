import express from 'express';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import userRoutes from './routes/userRoutes';

const app = express();

app.use(express.json());

const swaggerOptions = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Express TypeScript API',
            version: '1.0.0',
            description: 'A simple Express API built with TypeScript',
        },
        servers: [
            {
                url: `http://localhost:${process.env.SERVER_PORT || 3000}`,
            },
        ],
    },
    apis: ['./src/routes/*.ts'],
};

const swaggerDocs = swaggerJsdoc(swaggerOptions);

app.use(`/swagger-ui`, swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use('/api', userRoutes);

export default app;
