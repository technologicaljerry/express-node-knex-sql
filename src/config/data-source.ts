import { Pool } from 'pg';

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'swagger-typescript-express-node-postgresql',
    password: 'sfsroot',
    port: 5432,
});

export default pool;
